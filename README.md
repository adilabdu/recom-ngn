### Movie Recommendation Engine

Movie recommendation engine, built with data from TMDb `API`. A small project to learn about svelte-kit: a javascript framework built upon the svelte compiler.


#### Setup

Once you install dependencies with `npm install`; start the development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```
