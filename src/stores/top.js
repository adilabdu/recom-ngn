import { writable } from "svelte/store"

export const movies = writable([]);

// let page = 1
let page
export let total_pages
let KEY = '6aa35036dc79e3d1bd4cf8e6a7e6524a'

export const getTop = async (page) => {

    let BASE_URL = "https://api.themoviedb.org/3/discover/movie?page=" + page + "&sort_by=popularity.desc&api_key=" + KEY
    let res = await fetch(BASE_URL)
    let data = await res.json()
    
    data = data.results
    total_pages = data.total_pages

    const loaded_movies = data.map((data, index) => {
        
        return {
            index: index,
            id: data.id,
            title: data.title,
            overview: data.overview,
            poster: "https://image.tmdb.org/t/p/w500/" + data.poster_path,
            release_date: data.release_date ? data.release_date : "TBD",
            genre: data.genre_ids
        }
    });

    movies.set(loaded_movies)
}
